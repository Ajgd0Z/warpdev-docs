# Warpdev Docs

**Docs on the APIs used by Warpdev**

This project aims to assemble the necessary information related to the APIs used 
by [Warpdev].

Like [Warpdev] this documentation is being continually updated with the new 
features published by vendors and capabilities developed by [Warpcom].

## Warpcom

[Warpcom] positions itself as the market leader in the implementation of voice 
and data convergence solutions in IP, Unified Communications, Collaboration and 
Video, Contact Center, Mobility, IT Security, and Data Center and Cloud 
solutions.

Its offer of services includes auditing and technological consulting, the design 
of networks, provision of solutions, implementation, application integration and 
services managed in ICT solutions (total or partial outsourcing, remote
management, help desk, among others).

As an independent solution provider, we have partnerships with leading 
manufacturers to develop the most appropriate solutions for our customers.

[Warpcom] made a remarkable jorney in encouraging and cultivating innovation and 
creativity in Warpcom DNA and our customer transformation also. The proof of this
is the creation of our innovation and creativity Hub [Warpdev].


## Warpdev

[Warpdev] is the [Warpcom] Hub responsible for the development of custom 
product integrations. Leveraging the products' APIs, [Warpdev] wants to enable 
the digital transformation by automating the interconnections between systems.

## Warpdev Partners

From the [several partnerships][partners] Warpcom has with leading 
manufacturers the following are related to Warpdev:

* [Cisco], through its [DevNet] department
* [Alcatel-Lucent Enterprise][ale]
* [Altitude Software][altitude]


[Warpdev]: https://warpcom.com/en/solutions-technology/
[Warpcom]: https://warpcom.com/en/
[partners]: https://warpcom.com/en/partnerships/
[Cisco]: https://cisco.com/
[DevNet]: https://developer.cisco.com/
[ale]: https://www.al-enterprise.com/
[Rainbow]: https://www.openrainbow.com/
[altitude]: https://www.altitude.com/

## How to read these docs?

Two main subjects define the documentation:

1. **Manufacturer** - On the side menu you can find information, including products and solutions, regarding each Warpcom partner.
2. **Business Unit** - Warpcom has four BUs (The Network as a Platform, Collaboration and Costumer Experience, Cybersecurity & Public Safety, Data Center & Multi-Cloud), and each solution relates to one. Inside the manufacturer, we divided the products by BU. 

We call your attention to the following images:


<img alt="Deployed in WrpLab" src="./static/img/wrplab.png" title="Solution deployed in Warpcom Lab" style="width: 25px"> You'll find it next to solutions deployed in Warpcom Lab. 

<img alt="Used in Warpcom" src="./static/img/warpcom.png" title="Solution used in Warpcom" style="width: 25px"> You'll find it next to solutions used daily by Warpcom.


## Contacts

Have any doubt or want more information?
Check the [contact section on our website](https://warpcom.com/en/contactos/). By phone or e-mail don't forget to mention you want to talk with someone from Warpdev!